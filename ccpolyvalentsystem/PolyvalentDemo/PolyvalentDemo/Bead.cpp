#include "stdafx.h"
#include "Bead.h"

using namespace std;
using namespace cv;
Bead::Bead(Point2f center)
{
	location.x = center.x;
	location.y = center.y;
	dx = 30;
	dy = 0;
	
	blnStopped = false;
	id = 0;
	frame = 0;
}


Bead::~Bead()
{
}

cv::Point2f Bead::predict()
{
	frame++;
	return cv::Point2f(location.x + dx, location.y + dy);
}

void Bead::update(Point2f newpos)
{

	dx = newpos.x - location.x ;
	dy = newpos.y - location.y;

	location.x = newpos.x;
	location.y = newpos.y;
}
