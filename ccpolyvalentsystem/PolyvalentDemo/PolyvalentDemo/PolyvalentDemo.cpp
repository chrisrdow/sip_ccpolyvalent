// PolyvalentDemo.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Bead.h"
#include "Hungarian.h"
#include "MotionDetection.h"
#include "LuClassifier.h"
#include <iterator>
#include <map>

#define NCOEFFS 304
#define NCLS    7

using namespace cv;
using namespace std;

LuClassifier r_cls;

int64 class_count[NCLS] = { 0 };

template<class InputIt, class T = typename std::iterator_traits<InputIt>::value_type>
T most_common(InputIt begin, InputIt end)
{
	std::map<T, int> counts;
	for (InputIt it = begin; it != end; ++it) {
		if (counts.find(*it) != counts.end()) {
			++counts[*it];
		}
		else {
			counts[*it] = 1;
		}
	}
	return std::max_element(counts.begin(), counts.end(),
		[](const std::pair<T, int>& pair1, const std::pair<T, int>& pair2) {
		return pair1.second < pair2.second; })->first;
}

double GetCost(Point2f estPt, Point2f detPt, Point2f prevPt)
{
	double Q = sqrt(((960 / 4)*(960 / 4)) + ((600 / 4)*(600 / 4)));
	

	double dst = sqrt((estPt.x - detPt.x)*(estPt.x - detPt.x) + (estPt.y - detPt.y)*(estPt.y - detPt.y)) / Q;
	double dir = 0.5*(1 + ((abs(estPt.x - detPt.x))*(abs(estPt.y - detPt.y))) / sqrt((estPt.x*estPt.x + estPt.y*estPt.y) * (detPt.x*detPt.x + detPt.y*detPt.y)));

	if (dir < 0)
		dir = 0;

	//if ((abs(detPt.x - prevPt.x) < 2) && (abs(detPt.y - prevPt.y) < 2))
	//	return 0;
	double g = (exp(pow((estPt.x - detPt.x), 2) /  (96000.0))*exp(pow((estPt.y - detPt.y), 2) / (6000.0)));
	if (std::isinf(g))
		g = 1000;
	//return(dst*.40 + dir * .60);
	return (0.9*g + .05*dst + dir*.05);

}

void Erosion(int erosion_type, int erosion_size, Mat &e)
{
	Mat element = getStructuringElement(erosion_type,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));

	/// Apply the erosion operation
	erode(e, e, element);
	return;
}

void Dilation(int dilation_type, int dilation_size, Mat &d)
{
	Mat element = getStructuringElement(dilation_type,
		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		Point(dilation_size, dilation_size));

	/// Apply the dilation operation
	dilate(d, d, element);

	return;
}


void processImageSequence(std::string fname_r, std::string fname_g)
{
	std::ifstream ifiles_r;
	std::ifstream ifiles_g;
	std::string line_r;
	std::string line_g;
	std::stringstream convert;
	std::stringstream convert_d;
	std::string outscsv = "track_data.csv";

	int frameNumber = 1; /* The current frame number */
	int bead_count=0;
	cv::Mat image_r;                  /* Current frame. */
	cv::Mat image_g;
	Mat segmentationMap;        /* Will contain the segmentation map. This is the binary output map. */
	Mat bgr[3];

	MotionDetection mdet;
	SimpleBlobDetector::Params params;
	std::ofstream tr_data_csv;
	tr_data_csv.open(outscsv.c_str(), ios::app);

	// Change thresholds
	//params.minThreshold =50;
	//params.maxThreshold = 180;
	params.minDistBetweenBlobs = 1.5f;
	params.blobColor = 255;
	// Filter by Area.
	params.filterByArea = false;
	params.minArea = 1.3f;

	// Filter by Circularity
	params.filterByCircularity = false;
	params.minCircularity = 0.8f;

	// Filter by Convexity
	params.filterByConvexity = false;
	params.minConvexity = 1.0f;

	// Filter by Inertia
	params.filterByInertia = false;
	params.minInertiaRatio = 0.1f;
	// Set up the detector with default parameters.
	//SimpleBlobDetector detector;
	Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
		
	int b_number = 0;
	std::vector<Bead> beads;


	ifiles_r.open(fname_r, ifstream::in);
	ifiles_g.open(fname_g, ifstream::in);
	if ((ifiles_r.is_open())&& (ifiles_g.is_open()))
	{
		std::vector<Bead> currentFrameBeads;
		std::vector<Point2f> predBeadPts;
		std::vector<Bead> previousFrameBeads;
		std::vector<vector<double>> costMatrix;
		std::vector<int> assignment;
		std::set<int> unmatchedDetections;
		std::set<int> unmatchedTrajectories;
		std::set<int> allItems;
		std::set<int> matchedItems;
		std::vector<cv::Point> matchedPairs;

		unsigned int trkNum = 0;
		unsigned int detNum = 0;
		int bead_id = 0;


		while ((ifiles_g.good())&& (ifiles_r.good()))
		{
			cv::Mat image;
			getline(ifiles_r, line_r);
			getline(ifiles_g, line_g);
			
			image_r = imread(line_r, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
			image_g = imread(line_g, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);

            cvtColor(image_r, image, CV_BGR2GRAY);



			Mat frame(image.rows / 2, image.cols / 2, CV_16UC1);
			resize(image, frame, frame.size(), 0, 0, INTER_AREA);

			if (frameNumber == 1) {
				frame = cv::Scalar::all(0);
				segmentationMap = Mat(frame.rows, frame.cols, CV_8UC1, Scalar(0));
				mdet.libvibeModel_Sequential_AllocInit_16u_C1R((uint16_t *)frame.data, frame.cols, frame.rows);
				
			}
			
			mdet.libvibeModel_Sequential_Segmentation_16u_C1R((uint16_t *)frame.data, (uint8_t *)segmentationMap.data);
			mdet.libvibeModel_Sequential_Update_16u_C1R((uint16_t *)frame.data, (uint8_t *)segmentationMap.data);

			Erosion(MORPH_CROSS, 1, segmentationMap);
			Dilation(MORPH_ELLIPSE, 1, segmentationMap);

			std::vector<KeyPoint> keypoints;
			detector->detect(segmentationMap, keypoints);

			if (frameNumber == 1) {
				for (auto &kp : keypoints) {

					Bead b(kp.pt);

					b.id = bead_id++;
					previousFrameBeads.push_back(b);
				}
			}

			else {
				currentFrameBeads.clear();
				predBeadPts.clear();
			
				for (auto &pb : previousFrameBeads) {

					predBeadPts.push_back(pb.predict());
				}
				for (auto &kp : keypoints) {

					//if (kp.pt.x > 30) {
						Bead b(kp.pt);
						bool add_v = true;
						for (auto &pb : previousFrameBeads) {
							//	b.id = pb.id;
							float pbx = (float)pb.location.x;
							float pby = (float)pb.location.y;


							if ((abs(kp.pt.x - pbx) < 2) && (abs(kp.pt.y - pby) < 2))
							{
								//b.dx = pb.dx;
								add_v = false;
							}

						}
						if (add_v) {
							b.blnStopped = false;
						}
						else {
							b.blnStopped = true;
							b.dx = 0;
						}

						currentFrameBeads.push_back(b);

					//}

				}


				trkNum = (unsigned int)predBeadPts.size();
				detNum = (unsigned int)currentFrameBeads.size();
				   
				costMatrix.clear();
				costMatrix.resize(trkNum, vector<double>(detNum, 0));

				for (unsigned int i = 0; i < trkNum; i++) // compute iou matrix as a distance matrix
				{
					for (unsigned int j = 0; j < detNum; j++)
					{
						// use 1-iou because the hungarian algorithm computes a minimum-cost assignment.
						if ((!currentFrameBeads[j].blnStopped)&&(  currentFrameBeads[j].location.x >10 ) && (previousFrameBeads[i].location.x <950)){
							costMatrix[i][j] = GetCost(predBeadPts[i], currentFrameBeads[j].location, previousFrameBeads[i].location);
						}
						else {
							costMatrix[i][j] = 10000.0;
						}
					}
				}

				//compute assignment
				HungarianAlgorithm HungAlgo;
				assignment.clear();
				if(costMatrix.size() > 0)
					HungAlgo.Solve(costMatrix, assignment);

 
				unmatchedTrajectories.clear();
				unmatchedDetections.clear();
				allItems.clear();
				matchedItems.clear();

				if (detNum > trkNum) //	there are unmatched detections
				{
					for (unsigned int n = 0; n < detNum; n++)
						allItems.insert(n);

					for (unsigned int i = 0; i < trkNum; ++i)
						matchedItems.insert(assignment[i]);

					set_difference(allItems.begin(), allItems.end(),
						matchedItems.begin(), matchedItems.end(),
						insert_iterator<set<int>>(unmatchedDetections, unmatchedDetections.begin()));
				}
				else if (detNum < trkNum) // there are unmatched trajectory/predictions
				{
					for (unsigned int i = 0; i < trkNum; ++i)
						if (assignment[i] == -1) // unassigned label will be set as -1 in the assignment algorithm
							unmatchedTrajectories.insert(i);
				}
				else
					;

				matchedPairs.clear();
				for (unsigned int i = 0; i < trkNum; ++i)
				{
					if (assignment[i] == -1) // pass over invalid values
						continue;
					if (costMatrix[i][assignment[i]] > 1.0)
					{
						unmatchedTrajectories.insert(i);
						unmatchedDetections.insert(assignment[i]);
					}
					//else if (costMatrix[i][assignment[i]] < 3.0)
					//{
					//	unmatchedTrajectories.insert(i);
					//	unmatchedDetections.insert(assignment[i]);
					//}
					else
						matchedPairs.push_back(cv::Point(i, assignment[i]));
				}
			}

			Mat im_with_keypoints;
			drawKeypoints(segmentationMap, keypoints, im_with_keypoints, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
			int detIdx, trkIdx;
			convert_d << frameNumber;

			for (unsigned int i = 0; i < matchedPairs.size(); i++)
			{
				trkIdx = matchedPairs[i].x;
				detIdx = matchedPairs[i].y;
				
				if ((keypoints[detIdx].pt.x < 950) && (previousFrameBeads[trkIdx].location.x < 950) && (!previousFrameBeads[trkIdx].blnStopped)) {
					
					
					cv::Rect roi;
					roi.x = ((int)keypoints[detIdx].pt.x) * 2 - 4;
					roi.y = ((int)keypoints[detIdx].pt.y) * 2 - 4;

					roi.width = 10;
					roi.height = 10;
					int bclass = -1;
					if ((roi.x > 0) && (roi.y > 0) && (roi.x + roi.width < image_r.cols) && (roi.y + roi.height < image_r.rows)) {
						//cv::Mat bead = image_r(roi);
						//bclass = r_cls.classify(bead);
					}
					previousFrameBeads[trkIdx].cls.push_back(bclass);

					cv::line(image_r, previousFrameBeads[trkIdx].location*2, keypoints[detIdx].pt*2, Scalar(0, 65500, 0), 2, 8, 0);

					if (frameNumber > 3) {
						//cv::Point2f p_g = previousFrameBeads[trkIdx].location * 2 +  (previousFrameBeads[trkIdx].location * 2 - keypoints[detIdx].pt * 2)/2;
						//cv::Point2f p_g =  (keypoints[detIdx].pt * 2) + (previousFrameBeads[trkIdx].location * 2 - keypoints[detIdx].pt * 2) / 2;
						cv::Point2f p_g = (previousFrameBeads[trkIdx].location * 2) + (-previousFrameBeads[trkIdx].location * 3 + keypoints[detIdx].pt * 3);
						cv::Rect roi_g;
						roi_g.x = (int)(p_g.x - 10.0f);
						roi_g.y = (int)(p_g.y - 10.0f);
						roi_g.width = 20;
						roi_g.height = 20;
						std::stringstream conv;
						conv << "green_loc//g_";
						conv << frameNumber;
						conv << "_";
						conv << detIdx;
						conv << ".tiff";
						std::string out_gf = conv.str();

						vector<int> params = {
							259, 1//,     // No compression, turn off default LZW 
						};
						if ((roi_g.x > 0) && (roi_g.y > 0) && (roi_g.x + roi_g.width < image_g.cols)&&(roi_g.y + roi_g.height < image_g.rows)) {
							//cv::Mat bg = image_g(roi_g);
							//if (!(frameNumber % 15))
							//	imwrite(out_gf, bg, params);
							//cv::rectangle(image_g, roi_g, cv::Scalar(0, 65000, 0),2,8,0);
							//imwrite(out_gf, image_g, params);
						}
					}
					convert << bclass << "  " << previousFrameBeads[trkIdx].id << "  " << previousFrameBeads[trkIdx].dx;
					//cv::putText(image_r, convert.str(), keypoints[detIdx].pt*2, FONT_HERSHEY_PLAIN, 1.5, Scalar(65500, 65500, 0), 2, 8, false);

					convert.clear();
					convert.str(string());

					
					convert_d<< "," << previousFrameBeads[trkIdx].location.x * 2
						<< "," << previousFrameBeads[trkIdx].location.y * 2
						<< "," << previousFrameBeads[trkIdx].dx * 2 
						<< "," << previousFrameBeads[trkIdx].dy * 2
						<< "," << keypoints[detIdx].pt.x * 2 << "," 
						<< keypoints[detIdx].pt.y * 2;
				}
			}

			string row = convert_d.str();

			convert_d.clear();//clear any bits set
			convert_d.str(string());

			//tr_data_csv << row;
			//tr_data_csv << std::endl;

			if (frameNumber > 1) {
				vector<Bead> pfb = previousFrameBeads;
				previousFrameBeads.clear();
				for (unsigned int i = 0; i < matchedPairs.size(); i++)
				{
					trkIdx = matchedPairs[i].x;
					detIdx = matchedPairs[i].y;

					currentFrameBeads[detIdx].location.x = pfb[trkIdx].location.x;
					currentFrameBeads[detIdx].location.y = pfb[trkIdx].location.y;
					currentFrameBeads[detIdx].dx = pfb[trkIdx].dx;
					if ((abs(pfb[trkIdx].location.x - keypoints[detIdx].pt.x) < 2) && (abs(pfb[trkIdx].location.y - keypoints[detIdx].pt.y) < 2)){
						currentFrameBeads[detIdx].update(keypoints[detIdx].pt);
						currentFrameBeads[detIdx].blnStopped = true;
					}
					else{
						currentFrameBeads[detIdx].update(keypoints[detIdx].pt);
						currentFrameBeads[detIdx].id = pfb[trkIdx].id;
					 
						currentFrameBeads[detIdx].blnStopped = false;// pfb[trkIdx].blnStopped;
					}
					currentFrameBeads[detIdx].frame = pfb[trkIdx].frame + 1;
					currentFrameBeads[detIdx].cls = pfb[trkIdx].cls;
					 
					if (currentFrameBeads[detIdx].location.x < 950) {
						previousFrameBeads.push_back(currentFrameBeads[detIdx]);
					}
					else {
						if (currentFrameBeads[detIdx].cls.size() > 0){

							int cat = most_common(currentFrameBeads[detIdx].cls.begin(), currentFrameBeads[detIdx].cls.end());
							class_count[cat]++;
						}
							
					}
				}
				for (auto umd : unmatchedDetections)
				{

					Bead b(keypoints[umd].pt);
					bool add_v = true;
					for (auto &pb : pfb) {
						 
						float pbx = (float)pb.location.x;
						float pby = (float)pb.location.y;

						//
						if ((abs(keypoints[umd].pt.x - pbx) < 2) && (abs(keypoints[umd].pt.y - pby) < 2)) {
							
							//b.id = previousFrameBeads[umd].id;
							//b.id = pb.id;

							add_v = false;
						}

					}
					if (add_v) {
						b.blnStopped = true;
					}
					else {
						b.blnStopped = true;
					}

					if ((!b.blnStopped)) {
						b.id = bead_id++;
					}
					else {
						//b.dx = currentFrameBeads[umd].dx;
						//b.id = currentFrameBeads[umd].id;
						//b.blnStopped = currentFrameBeads[umd].blnStopped;
					}

					//if (b.location.x < 10) {
					//	b.frame = -1;
					//}

					if ( (b.location.x < 950)) {
						previousFrameBeads.push_back(b);
					}

					//else {
					//	bead_count++;
					//}
				}


				convert << line <<  "  " << currentFrameBeads.size() << " " << bead_count;
				string hd = convert.str();
				convert.clear();//clear any bits set
				convert.str(string());
				namedWindow("Segmentation by ViBe", CV_WINDOW_NORMAL);// Create a window for display.
																	  //resizeWindow("Segmentation by ViBe", (int)(segmentationMap.cols / 4), (int)(segmentationMap.rows / 4));


				convert << "  " << class_count[0] << "  " << class_count[1] << "  "
					<< class_count[2] << "  " << class_count[3] << "  " 
					<< class_count[4] << "  " << class_count[5] << "  " 
					<< class_count[6];

				string beadcount = convert.str();
				convert.clear();//clear any bits set
				convert.str(string());



				resizeWindow("Segmentation by ViBe", (int)(image_r.cols*.7), (int)(image_r.rows*.7));
				imshow("Segmentation by ViBe", /*segmentationMap*/ image_r /*im_with_keypoints*/);
				waitKey(1);
			}
			//cv::waitKey(2);
			frameNumber++;

		}
	}
	tr_data_csv.close();
}

int main(int argc, char * argv[])
{
	if (argc < 2) {
		cout << "enter an input file name" << endl;
		return 1;
	}
	//#define NCOEFFs 303
	//#define NCLS    7
	r_cls.init("seven_cls_no_74_1.csv", NCLS, NCOEFFS, 20, 20);
	system("md green_loc");
	processImageSequence(std::string(argv[1]), std::string(argv[2]) );

    return 0;
}

