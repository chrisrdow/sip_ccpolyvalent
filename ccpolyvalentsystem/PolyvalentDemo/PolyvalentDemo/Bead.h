#pragma once
class Bead
{
public:
	Bead(cv::Point2f center);
	~Bead();
	cv::Point2f predict();
	
public:
	cv::Point2f location;
	void update(cv::Point2f newpos);
	int id;
	int frame;
	bool blnStopped;
	float dx;
	float dy;
	std::vector<int> cls;


};

