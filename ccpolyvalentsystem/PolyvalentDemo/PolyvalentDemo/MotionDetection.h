#pragma once
#define NUMBER_OF_HISTORY_IMAGES 2
#define COLOR_BK   0 /*!< Default label for background pixels */
#define COLOR_FOREGROUND 255 /*!< Default label for foreground pixels. Note that some authors chose any value different from 0 instead */

struct vibeModel_Sequential
{
	/* Parameters. */
	uint32_t width;
	uint32_t height;
	uint32_t numberOfSamples;
	uint32_t matchingThreshold;
	uint32_t matchingNumber;
	uint32_t updateFactor;

	/* Storage for the history. */
	uint16_t *historyImage;
	uint16_t *historyBuffer;
	uint32_t lastHistoryImageSwapped;

	/* Buffers with random values. */
	uint32_t *jump;
	int *neighbor;
	uint32_t *position;
};
typedef struct vibeModel_Sequential vibeModel_Sequential_t;

class MotionDetection
{
public:
	MotionDetection();
	~MotionDetection();
private:
	vibeModel_Sequential_t * model;
public:
	int32_t libvibeModel_Sequential_AllocInit_16u_C1R(
		const uint16_t *image_data,
		const uint32_t width,
		const uint32_t height
	);

	int32_t libvibeModel_Sequential_Segmentation_16u_C1R(
		const uint16_t *image_data,
		uint8_t *segmentation_map
	);

	int32_t libvibeModel_Sequential_Update_16u_C1R(
		const uint16_t *image_data,
		uint8_t *updating_mask
	);

};

