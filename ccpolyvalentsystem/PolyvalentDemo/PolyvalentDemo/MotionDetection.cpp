#include "stdafx.h"
#include "MotionDetection.h"

static inline int abs_uint(const int i)
{
	return (i >= 0) ? i : -i;
}
static inline int32_t distance_is_close_16u_C3R(uint16_t r1, uint16_t g1, uint16_t b1, uint16_t r2, uint16_t g2, uint16_t b2, uint32_t threshold)
{
	return (abs_uint(r1 - r2) + abs_uint(g1 - g2) + abs_uint(b1 - b2) <= 4.2 * threshold);
}


MotionDetection::MotionDetection()
{
	
	model = (vibeModel_Sequential_t*)calloc(1, sizeof(*model));
	assert(model != NULL);

	/* Default parametersB values. */
	model->numberOfSamples = 25;
	model->matchingThreshold = 4000;
	model->matchingNumber = 6;
	model->updateFactor = 6060;

	/* Storage for the history. */
	model->historyImage = NULL;
	model->historyBuffer = NULL;
	model->lastHistoryImageSwapped = 0;

	/* Buffers with random values. */
	model->jump = NULL;
	model->neighbor = NULL;
	model->position = NULL;

}


MotionDetection::~MotionDetection()
{
	if (model == NULL)
		return;

	if (model->historyBuffer == NULL) {
		free(model);
		return;
	}

	free(model->historyImage);
	free(model->historyBuffer);
	free(model->jump);
	free(model->neighbor);
	free(model->position);
	free(model);
}

int32_t MotionDetection::libvibeModel_Sequential_AllocInit_16u_C1R(const uint16_t * image_data, const uint32_t width, const uint32_t height)
{
	assert((image_data != NULL) && (model != NULL));
	assert((width > 0) && (height > 0));

	/* Finish model alloc - parameters values cannot be changed anymore. */
	model->width = width;
	model->height = height;

	/* Creates the historyImage structure. */
	model->historyImage = NULL;
	model->historyImage = (uint16_t*)malloc(NUMBER_OF_HISTORY_IMAGES * width * height * sizeof(uint16_t));

	assert(model->historyImage != NULL);

	for (int i = 0; i < NUMBER_OF_HISTORY_IMAGES; ++i) {
		for (int index = width * height - 1; index >= 0; --index)
			model->historyImage[i * width * height + index] = image_data[index];
	}

	/* Now creates and fills the history buffer. */
	model->historyBuffer = (uint16_t*)malloc(width * height * (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES) * sizeof(uint16_t));
	assert(model->historyBuffer != NULL);

	for (int index = width * height - 1; index >= 0; --index) {
		uint16_t value = image_data[index];

		for (int x = 0; x < (int)model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES; ++x) {
			int value_plus_noise = (int)value + rand() % (int)((15.0 / 255.0) * 60000) - (int)((10.0 / 255.0) * 60000);

			if (value_plus_noise < 0) { value_plus_noise = 0; }
			if (value_plus_noise > 60000) { value_plus_noise = 60000; }

			model->historyBuffer[index * (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES) + x] = value_plus_noise;
		}
	}

	/* Fills the buffers with random values. */
	int size = (width > height) ? 2 * width + 1 : 2 * height + 1;

	model->jump = (uint32_t*)malloc(size * sizeof(*(model->jump)));
	assert(model->jump != NULL);

	model->neighbor = (int*)malloc(size * sizeof(*(model->neighbor)));
	assert(model->neighbor != NULL);

	model->position = (uint32_t*)malloc(size * sizeof(*(model->position)));
	assert(model->position != NULL);

	for (int i = 0; i < size; ++i) {
		model->jump[i] = (rand() % (2 * model->updateFactor)) + 1;            // Values between 1 and 2 * updateFactor.
		model->neighbor[i] = ((rand() % 3) - 1) + ((rand() % 3) - 1) * width / 240; // Values between { -width - 1, ... , width + 1 }.
		model->position[i] = rand() % (model->numberOfSamples);               // Values between 0 and numberOfSamples - 1.
	}

	return(0);

}

int32_t MotionDetection::libvibeModel_Sequential_Segmentation_16u_C1R(const uint16_t * image_data, uint8_t * segmentation_map)
{
	/* Basic checks. */
	assert((image_data != NULL) && (model != NULL) && (segmentation_map != NULL));
	assert((model->width > 0) && (model->height > 0));
	assert(model->historyBuffer != NULL);
	assert((model->jump != NULL) && (model->neighbor != NULL) && (model->position != NULL));

	/* Some variables. */
	uint32_t width = model->width;
	uint32_t height = model->height;
	uint32_t matchingNumber = model->matchingNumber;
	uint32_t matchingThreshold = model->matchingThreshold;

	uint16_t *historyImage = model->historyImage;
	uint16_t *historyBuffer = model->historyBuffer;

	/* Segmentation. */
	//memset(segmentation_map, matchingNumber - 1, width * height);

	for (int i = 0; i < (int)(width*height); i++)
		segmentation_map[i] = matchingNumber - 1;

	/* First history Image structure. */
	for (int index = (int)(width * height - 1); index >= 0; --index) {
		if (abs_uint((int)(image_data[index] - historyImage[index])) >(int)matchingThreshold)
			segmentation_map[index] = matchingNumber;
	}

	/* Next historyImages. */
	for (int i = 1; i < NUMBER_OF_HISTORY_IMAGES; ++i) {
		uint16_t *pels = &historyImage[i * width * height];

		for (int index = width * height - 1; index >= 0; --index) {
			if (abs_uint(image_data[index] - pels[index]) <= (int)matchingThreshold)
				--segmentation_map[index];
		}
	}

	/* For swapping. */
	model->lastHistoryImageSwapped = (model->lastHistoryImageSwapped + 1) % NUMBER_OF_HISTORY_IMAGES;
	uint16_t *swappingImageBuffer = &historyImage[(model->lastHistoryImageSwapped) * width * height];

	/* Now, we move in the buffer and leave the historyImages. */
	int numberOfTests = (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES);

	for (int index = width * height - 1; index >= 0; --index) {
		if (segmentation_map[index] > 0) {
			/* We need to check the full border and swap values with the first or second historyImage.
			* We still need to find a match before we can stop our search.
			*/
			uint32_t indexHistoryBuffer = index * numberOfTests;
			uint16_t currentValue = image_data[index];

			for (int i = numberOfTests; i > 0; --i, ++indexHistoryBuffer) {
				if (abs_uint(currentValue - historyBuffer[indexHistoryBuffer]) <= (int)matchingThreshold) {
					--segmentation_map[index];

					/* Swaping: Putting found value in history image buffer. */
					uint16_t temp = swappingImageBuffer[index];
					swappingImageBuffer[index] = historyBuffer[indexHistoryBuffer];
					historyBuffer[indexHistoryBuffer] = temp;

					/* Exit inner loop. */
					if (segmentation_map[index] <= 0) break;
				}
			} // for
		} // if
	} // for

	  /* Produces the output. Note that this step is application-dependent. */
	  //for (uint16_t *mask = segmentation_map; mask < segmentation_map + (width * height); ++mask)
	  //	if (*mask > 0) *mask = COLOR_FOREGROUND;

	for (int i = 0; i < (int)(width*height); i++) {
		if (segmentation_map[i] > 0) segmentation_map[i] = COLOR_FOREGROUND;
	}

	return(0);

}

int32_t MotionDetection::libvibeModel_Sequential_Update_16u_C1R(const uint16_t * image_data, uint8_t * updating_mask)
{
	assert((image_data != NULL) && (model != NULL) && (updating_mask != NULL));
	assert((model->width > 0) && (model->height > 0));
	assert(model->historyBuffer != NULL);
	assert((model->jump != NULL) && (model->neighbor != NULL) && (model->position != NULL));

	/* Some variables. */
	uint32_t width = model->width;
	uint32_t height = model->height;

	uint16_t *historyImage = model->historyImage;
	uint16_t *historyBuffer = model->historyBuffer;

	/* Some utility variable. */
	int numberOfTests = (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES);

	/* Updating. */
	uint32_t *jump = model->jump;
	int *neighbor = model->neighbor;
	uint32_t *position = model->position;

	/* All the frame, except the border. */
	uint32_t shift, indX, indY;
	int x, y;

	for (y = 1; y < (int)(height - 1); ++y) {
		shift = rand() % width;
		indX = jump[shift]; // index_jump should never be zero (> 1).

		while (indX < width - 1) {
			int index = indX + y * width;

			if (updating_mask[index] == COLOR_BACKGROUND) {
				/* In-place substitution. */
				uint16_t value = image_data[index];
				int index_neighbor = index + neighbor[shift];

				if (position[shift] < NUMBER_OF_HISTORY_IMAGES) {
					historyImage[index + position[shift] * width * height] = value;
					historyImage[index_neighbor + position[shift] * width * height] = value;
				}
				else {
					int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
					historyBuffer[index * numberOfTests + pos] = value;
					historyBuffer[index_neighbor * numberOfTests + pos] = value;
				}
			}

			++shift;
			indX += jump[shift];
		}
	}

	/* First row. */
	y = 0;
	shift = rand() % width;
	indX = jump[shift]; // index_jump should never be zero (> 1).

	while (indX <= width - 1) {
		int index = indX + y * width;

		if (updating_mask[index] == COLOR_BK) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indX += jump[shift];
	}

	/* Last row. */
	y = height - 1;
	shift = rand() % width;
	indX = jump[shift]; // index_jump should never be zero (> 1).

	while (indX <= width - 1) {
		int index = indX + y * width;

		if (updating_mask[index] == COLOR_BACKGROUND) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indX += jump[shift];
	}

	/* First column. */
	x = 0;
	shift = rand() % height;
	indY = jump[shift]; // index_jump should never be zero (> 1).

	while (indY <= height - 1) {
		int index = x + indY * width;

		if (updating_mask[index] == COLOR_BACKGROUND) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indY += jump[shift];
	}

	/* Last column. */
	x = width - 1;
	shift = rand() % height;
	indY = jump[shift]; // index_jump should never be zero (> 1).

	while (indY <= height - 1) {
		int index = x + indY * width;

		if (updating_mask[index] == COLOR_BACKGROUND) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indY += jump[shift];
	}

	/* The first pixel! */
	if (rand() % model->updateFactor == 0) {
		if (updating_mask[0] == 0) {
			int position = rand() % model->numberOfSamples;

			if (position < NUMBER_OF_HISTORY_IMAGES)
				historyImage[position * width * height] = image_data[0];
			else {
				int pos = position - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[pos] = image_data[0];
			}
		}
	}

	return(0);
}
