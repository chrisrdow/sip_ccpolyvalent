#pragma once
class LuClassifier
{
public:
	LuClassifier();
	int init(std::string fname, int ncls, int nc,int i_rows, int i_cols);
	int classify(cv::Mat bead);

	~LuClassifier();

private:
	int readCoeffs(int nclasses, int ncoeffs);
	double *B;
	double *v;
	void compute_DWT(cv::Mat bead, cv::Mat & i_decomp);
	void computeFeatureVect(cv::Mat Bead);
	std::string coeff_filename;
	int len;
	int xx_v;
	int yy_v;
	int nclass;
	int ncoeffs;
	int nvars;
	int computeLogit();
};

