#include "stdafx.h"
#include "LuClassifier.h"

using namespace std;
using namespace cv;
LuClassifier::LuClassifier()
{
}

int LuClassifier::init(std::string fname,int ncls, int nco,int rows, int cols)
{
	xx_v = rows/2;
	yy_v = cols/2;
	nvars = xx_v * yy_v * 3  + 1 + 1 + 1 + 1 + 1 - 2;

	v = new double[nvars];    // number of wv coeffs
	B = new double[nvars + 1];// number of logistic coeffs len + intercept

	ncoeffs = nco;
	nclass = ncls;
	coeff_filename = fname;

	return readCoeffs(ncls,nco);
}

int LuClassifier::classify(cv::Mat bead)
{
	 
	computeFeatureVect(bead);
	
	return computeLogit();
}


LuClassifier::~LuClassifier()
{
	delete B;
	delete v;
}

int LuClassifier::readCoeffs(int nclasses, int ncoeffs)
{
	ifstream ifs;
	string line;
	stringstream convert;

	int nvects = nclasses;
	//int nvars = ncoeffs + 1; // one added for Bo the intercept.

	B = new double[ncoeffs*nclasses];

	ifs.open(coeff_filename.c_str(), ifstream::in);
	if (ifs.is_open())
	{
		getline(ifs, line);
		getline(ifs, line);
		getline(ifs, line);

		int jj = 0;


		while (ifs.good())
		{
			string delimiter = ",";
			size_t pos = 0;
			double value = 0.0;
			int i = 0;
			string token;

			getline(ifs, line);


			while (((pos = line.find(delimiter)) != string::npos) && (i <nvects)) {


				string token = line.substr(0, pos);
				line.erase(0, pos + delimiter.length());
				stringstream convert(token);


				convert >> B[jj*nvects + i];
				i++;
			}

			jj++;
		}

	}
	else {
		return -1;
	}
	ifs.close();

	return 0;

	return 0;
}

void LuClassifier::compute_DWT(cv::Mat bead, cv::Mat& i_decomp)
{


		Mat im1, im2, im3, im4, im5, im6;
		double a, b, c, d;

		im1 = Mat::zeros(bead.rows / 2, bead.cols, CV_32F);
		im2 = Mat::zeros(bead.rows / 2, bead.cols, CV_32F);
		im3 = Mat::zeros(bead.rows / 2, bead.cols / 2, CV_32F);
		im4 = Mat::zeros(bead.rows / 2, bead.cols / 2, CV_32F);
		im5 = Mat::zeros(bead.rows / 2, bead.cols / 2, CV_32F);
		im6 = Mat::zeros(bead.rows / 2, bead.cols / 2, CV_32F);

		for (int rcnt = 0; rcnt< bead.rows; rcnt += 2)
		{
			for (int ccnt = 0; ccnt< bead.cols; ccnt++)
			{

				a = bead.at<float>(rcnt, ccnt);
				b = bead.at<float>(rcnt + 1, ccnt);
				c = (a + b)*0.707;
				d = (a - b)*0.707;
				int _rcnt = rcnt / 2;
				im1.at<float>(_rcnt, ccnt) = (float)c;
				im2.at<float>(_rcnt, ccnt) = (float)d;
			}
		}

		for (int rcnt = 0; rcnt< bead.rows / 2; rcnt++)
		{
			for (int ccnt = 0; ccnt< bead.cols; ccnt += 2)
			{

				a = im1.at<float>(rcnt, ccnt);
				b = im1.at<float>(rcnt, ccnt + 1);
				c = (a + b)*0.707;
				d = (a - b)*0.707;
				int _ccnt = ccnt / 2;
				im3.at<float>(rcnt, _ccnt) = (float)c;
				im4.at<float>(rcnt, _ccnt) = (float)d;
			}
		}

		for (int rcnt = 0; rcnt< bead.rows / 2; rcnt++)
		{
			for (int ccnt = 0; ccnt< bead.cols; ccnt += 2)
			{

				a = im2.at<float>(rcnt, ccnt);
				b = im2.at<float>(rcnt, ccnt + 1);
				c = (a + b)*0.707;
				d = (a - b)*0.707;
				int _ccnt = ccnt / 2;
				im5.at<float>(rcnt, _ccnt) = (float)c;
				im6.at<float>(rcnt, _ccnt) = (float)d;
			}
		}

		i_decomp = Mat::zeros(bead.rows, bead.cols, CV_32F);

		im3.copyTo(i_decomp(Rect(0, 0, bead.rows / 2, bead.cols / 2)));
		im4.copyTo(i_decomp(Rect(0, bead.cols / 2, bead.rows / 2, bead.cols / 2)));
		im5.copyTo(i_decomp(Rect(bead.rows / 2, 0, bead.rows / 2, bead.cols / 2)));
		im6.copyTo(i_decomp(Rect(bead.rows / 2, bead.cols / 2, bead.rows / 2, bead.cols / 2)));

		return;

}

void LuClassifier::computeFeatureVect(cv::Mat Bead)
{


	Mat i_f32(Bead.cols, Bead.rows, CV_32F);
	Mat dwtr(Bead.cols, Bead.rows, CV_32F);
	Mat dwtg(Bead.cols, Bead.rows, CV_32F);
	Mat dwtb(Bead.cols, Bead.rows, CV_32F);

	Mat Bgrf[3];
	Rect wvRoi;

	Bead.convertTo(i_f32, CV_32FC3, 1.0 / 65535.0, 0.0);
	split(i_f32, Bgrf);
	compute_DWT(Bgrf[2], dwtr);
	compute_DWT(Bgrf[1], dwtg);
	compute_DWT(Bgrf[0], dwtb);

	wvRoi.x = 0;
	wvRoi.y = 0;
	wvRoi.width =  dwtr.cols / 2;
	wvRoi.height = dwtr.rows / 2;

	Mat  dwt2r =  dwtr(wvRoi);
	Mat  dwt2g =  dwtg(wvRoi);
	Mat  dwt2b =  dwtb(wvRoi);

	int ii = 0;
	double x = (double)dwt2b.at<float>(0, 0);
	double y = (double)dwt2b.at<float>(0, 1);

	v[ii++] = (double)x;
	v[ii++] = (double)y;
	v[ii++] = (double)x*x;
	v[ii++] = (double)y*y;
	v[ii++] = (double)x*y;
	
	 
	for (int j = 0; j < dwt2r.rows; j++) {
		for (int i = 0; i < dwt2r.cols; i++) {

			v[ii++] = (double)dwt2r.at<float>(i, j)*65535.0;

		}
	}

	for (int j = 0; j < dwt2g.rows; j++) {
		for (int i = 0; i < dwt2g.cols; i++) {

			v[ii++] = (double)dwt2g.at<float>(i, j)*65535.0;

		}
	}
	for (int j = 2; j < dwt2b.rows; j++) {
		for (int i = 0; i < 1; i++) {
			v[ii++] = (double)dwt2b.at<float>(i, j)*65535.0;
		}
	}

	for (int j = 0; j < dwt2b.rows; j++) {
		for (int i = 1; i < dwt2g.cols; i++) {
			v[ii++] = (double)dwt2b.at<float>(i, j)*65535.0;
		}
	}

	return;

}

int LuClassifier::computeLogit()
{
	vector<double> p_i;
	double s_exp = 0.0;

	double * L = new double[nclass];
	for (int i = 0; i < nclass; i++) {
		L[i] = 0;
		L[i] = B[i];
	}


	for (int j = 0; j < nclass; j++) {

		for (int i = 1; i < nvars + 1; i++) {
			L[j] += v[i - 1] * B[(i)*nclass + j];
		}

		s_exp += exp(L[j]);
	}

	for (int i = 0; i < nclass; i++) {
		p_i.push_back(exp(L[i])/s_exp);
	}

	double p = 0.0;
	int cls = -1;
	for (int i = 0; i < p_i.size(); i++) {
		if (p_i[i] > p) {
			p = p_i[i];
			cls = i;
		}
	}
	delete L;
	return cls;
}
