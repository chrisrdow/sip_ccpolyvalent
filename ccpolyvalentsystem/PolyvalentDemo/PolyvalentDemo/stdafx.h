// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>


#include <math.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>

#include <iostream>
#include <fstream>      // std::ifstream
#include <string>
#include <time.h>        
#include <Windows.h>
#include <queue>
#include <list>
#include <set>
#include <iterator>



// TODO: reference additional headers your program requires here
