
#include "stdafx.h"
#include "Blob.h"
#include "Hungarian.h"
#include <math.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>

#include <iostream>
#include <fstream>      // std::ifstream
#include <string>
#include <time.h>        
#include <Windows.h>
#include <queue>
#include <list>
#include <set>
#include <iterator>

#define PI (float)3.1415926535897



using namespace cv;
using namespace std;

#define NUMBER_OF_HISTORY_IMAGES 2
#define COLOR_BK   0 /*!< Default label for background pixels */
#define COLOR_FOREGROUND 255 /*!< Default label for foreground pixels. Note that some authors chose any value different from 0 instead */

bool sortByindex(const Point2i &lhs, const Point2i &rhs) { return lhs.y < rhs.y; }
double GetCost(Point2f estPt, Point2f detPt)
{
	double Q = sqrt(((1920 / 4)*(1920 / 4)) + ((1200 / 4)*(1200 / 4)));


	double dst = sqrt((estPt.x - detPt.x)*(estPt.x - detPt.x) + (estPt.y - detPt.y)*(estPt.y - detPt.y)) / Q;
	double dir = 0.5*(1 + ((estPt.x - detPt.x)*-10.0) / sqrt((estPt.x*estPt.x + estPt.y*estPt.y) * (detPt.x*detPt.x + detPt.y*detPt.y)));
	//	if (dst > 2)
	//		return 0;
	//	if (dst < .005)
	//		return 0;
	//	if (abs(estPt.x - detPt.x) > 20)
	//		return 0;
	if (dir < 0)
		dir = 0;
	//double del = abs(abs(estPt.x - detPt.x) - dx) + abs(abs(estPt.y - detPt.y)-dy);
	return(dst*.70 + dir*.40);
	//return (double)(dst);
}



class BipGraph
{
	// m and n are number of vertices on left
	// and right sides of Bipartite Graph
	int m, n;

	// adj[u] stores adjacents of left side
	// vertex 'u'. The value of u ranges from 1 to m.
	// 0 is used for dummy vertex
	list<int> *adj;

	// These are basically pointers to arrays needed
	// for hopcroftKarp()
	int *pairU, *pairV, *dist;

public:
	BipGraph(int m, int n); // Constructor
	void addEdge(int u, int v); // To add edge

								// Returns true if there is an augmenting path
	bool bfs();

	// Adds augmenting path if there is one beginning
	// with u
	bool dfs(int u);

	// Returns size of maximum matcing
	int hopcroftKarp();
};

// Returns size of maximum matching
int BipGraph::hopcroftKarp()
{
	// pairU[u] stores pair of u in matching where u
	// is a vertex on left side of Bipartite Graph.
	// If u doesn't have any pair, then pairU[u] is NIL
	pairU = new int[m + 1];

	// pairV[v] stores pair of v in matching. If v
	// doesn't have any pair, then pairU[v] is NIL
	pairV = new int[n + 1];

	// dist[u] stores distance of left side vertices
	// dist[u] is one more than dist[u'] if u is next
	// to u'in augmenting path
	dist = new int[m + 1];

	// Initialize NIL as pair of all vertices
	for (int u = 0; u < m; u++)
		pairU[u] = NULL;
	for (int v = 0; v < n; v++)
		pairV[v] = NULL;

	// Initialize result
	int result = 0;

	// Keep updating the result while there is an
	// augmenting path.
	while (bfs())
	{
		// Find a free vertex
		for (int u = 1; u <= m; u++)

			// If current vertex is free and there is
			// an augmenting path from current vertex
			if (pairU[u] == NULL && dfs(u))
				result++;
	}
	return result;
}

// Returns true if there is an augmenting path, else returns
// false
bool BipGraph::bfs()
{

	queue<int> Q; //an integer queue

				  // First layer of vertices (set distance as 0)
	for (int u = 1; u <= m; u++)
	{
		// If this is a free vertex, add it to queue
		if (pairU[u] == NULL)
		{
			// u is not matched
			dist[u] = 0;
			Q.push(u);
		}

		// Else set distance as infinite so that this vertex
		// is considered next time
		else dist[u] = INT_MAX;
	}

	// Initialize distance to NIL as infinite
	dist[0] = INT_MAX;

	// Q is going to contain vertices of left side only. 
	while (!Q.empty())
	{
		// Dequeue a vertex
		int u = Q.front();
		Q.pop();

		// If this node is not NIL and can provide a shorter path to NIL
		if (dist[u] < dist[0])
		{
			// Get all adjacent vertices of the dequeued vertex u
			list<int>::iterator i;
			for (i = adj[u].begin(); i != adj[u].end(); ++i)
			{
				int v = *i;

				// If pair of v is not considered so far
				// (v, pairV[V]) is not yet explored edge.
				if (dist[pairV[v]] == INT64_MAX)
				{
					// Consider the pair and add it to queue
					dist[pairV[v]] = dist[u] + 1;
					Q.push(pairV[v]);
				}
			}
		}
	}

	// If we could come back to NIL using alternating path of distinct
	// vertices then there is an augmenting path
	return (dist[0] != INT64_MAX);
}

// Returns true if there is an augmenting path beginning with free vertex u
bool BipGraph::dfs(int u)
{
	if (u != 0)
	{
		list<int>::iterator i;
		for (i = adj[u].begin(); i != adj[u].end(); ++i)
		{
			// Adjacent to u
			int v = *i;

			// Follow the distances set by BFS
			if (dist[pairV[v]] == dist[u] + 1)
			{
				// If dfs for pair of v also returns
				// true
				if (dfs(pairV[v]) == true)
				{
					pairV[v] = u;
					pairU[u] = v;
					return true;
				}
			}
		}

		// If there is no augmenting path beginning with u.
		dist[u] = INT_MAX;
		return false;
	}
	return true;
}

// Constructor
BipGraph::BipGraph(int m, int n)
{
	this->m = m;
	this->n = n;
	adj = new list<int>[m + 1];
}

// To add edge from u to v and v to u
void BipGraph::addEdge(int u, int v)
{
	adj[u].push_back(v); // Add u to v’s list.
	adj[v].push_back(u); // Add u to v’s list.
}






#define SHOW_STEPS            // un-comment or comment this line to show steps or not

// global variables ///////////////////////////////////////////////////////////////////////////////
const cv::Scalar SCALAR_BLACK = cv::Scalar(0.0, 0.0, 0.0);
const cv::Scalar SCALAR_WHITE = cv::Scalar(255.0, 255.0, 255.0);
const cv::Scalar SCALAR_YELLOW = cv::Scalar(0.0, 255.0, 255.0);
const cv::Scalar SCALAR_GREEN = cv::Scalar(0.0, 200.0, 0.0);
const cv::Scalar SCALAR_RED = cv::Scalar(0.0, 0.0, 255.0);

// function prototypes ////////////////////////////////////////////////////////////////////////////
void matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &existingBlobs, std::vector<Blob> &currentFrameBlobs);
void addBlobToExistingBlobs(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs, int &intIndex);
void addNewBlob(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs);
double distanceBetweenPoints(cv::Point point1, cv::Point point2);
void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName);
void drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName);
void drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy);


void matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &existingBlobs, std::vector<Blob> &currentFrameBlobs) {

	for (auto &existingBlob : existingBlobs) {

		existingBlob.blnCurrentMatchFoundOrNewBlob = false;

		existingBlob.predictNextPosition();
	}

	for (auto &currentFrameBlob : currentFrameBlobs) {

		int intIndexOfLeastDistance = 0;
		double dblLeastDistance = 100000.0;

		for (unsigned int i = 0; i < existingBlobs.size(); i++) {
			if (existingBlobs[i].blnStillBeingTracked == true) {
				double dblDistance = distanceBetweenPoints(currentFrameBlob.centerPositions.back(), existingBlobs[i].predictedNextPosition);

				if ((dblDistance < dblLeastDistance) && (dblDistance < 50) && (dblDistance >= 2)) {
					dblLeastDistance = dblDistance;
					intIndexOfLeastDistance = i;
				}
				else if (dblDistance < 1) {
					currentFrameBlob.blnStopped = true;
					intIndexOfLeastDistance = i;
				}
			}
		}
		//addBlobToExistingBlobs(currentFrameBlob, existingBlobs, intIndexOfLeastDistance);

		if (dblLeastDistance < currentFrameBlob.dblCurrentDiagonalSize * 1.15) {
			addBlobToExistingBlobs(currentFrameBlob, existingBlobs, intIndexOfLeastDistance);
		}
		else {
			addNewBlob(currentFrameBlob, existingBlobs);
		}

	}

	for (auto &existingBlob : existingBlobs) {

		if (existingBlob.blnCurrentMatchFoundOrNewBlob == false) {
			existingBlob.intNumOfConsecutiveFramesWithoutAMatch++;
		}

		if (existingBlob.intNumOfConsecutiveFramesWithoutAMatch >= 2) {
			existingBlob.blnStillBeingTracked = false;
		}

	}

}

///////////////////////////////////////////////////////////////////////////////////////////////////
void addBlobToExistingBlobs(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs, int &intIndex) {

	//	existingBlobs[intIndex].currentContour = currentFrameBlob.currentContour;
	//	existingBlobs[intIndex].currentBoundingRect = currentFrameBlob.currentBoundingRect;

	existingBlobs[intIndex].centerPositions.push_back(currentFrameBlob.centerPositions.back());

	//	existingBlobs[intIndex].dblCurrentDiagonalSize = currentFrameBlob.dblCurrentDiagonalSize;
	//	existingBlobs[intIndex].dblCurrentAspectRatio = currentFrameBlob.dblCurrentAspectRatio;

	existingBlobs[intIndex].blnStillBeingTracked = true;
	existingBlobs[intIndex].blnCurrentMatchFoundOrNewBlob = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void addNewBlob(Blob &currentFrameBlob, std::vector<Blob> &existingBlobs) {

	currentFrameBlob.blnCurrentMatchFoundOrNewBlob = true;

	existingBlobs.push_back(currentFrameBlob);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
double distanceBetweenPoints(cv::Point point1, cv::Point point2) {

	int intX = abs(point1.x - point2.x);
	int intY = abs(point1.y - point2.y);

	//return(point2.x - point1.x + abs(point1.y - point2.y));
	return(sqrt(pow(intX, 2) + pow(intY, 2)));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName) {
	cv::Mat image(imageSize, CV_8UC3, SCALAR_BLACK);

	cv::drawContours(image, contours, -1, SCALAR_WHITE, -1);

	cv::imshow(strImageName, image);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName) {

	cv::Mat image(imageSize, CV_8UC3, SCALAR_BLACK);

	std::vector<std::vector<cv::Point> > contours;

	//for (auto &blob : blobs) {
	//	if (blob.blnStillBeingTracked == true) {
	//		contours.push_back(blob.currentContour);
	//	}
	//}

	cv::drawContours(image, contours, -1, SCALAR_WHITE, -1);

	cv::imshow(strImageName, image);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy) {

	for (unsigned int i = 0; i < blobs.size(); i++) {

		if ((blobs[i].blnStillBeingTracked == true) && (blobs[i].blnStopped == false)) {
			cv::rectangle(imgFrame2Copy, blobs[i].currentBoundingRect, SCALAR_GREEN, 1);

			int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
			//double dblFontScale = blobs[i].dblCurrentDiagonalSize / 60.0;
			double dblFontScale = 30.0 / 60.0;

			int intFontThickness = (int)std::round(dblFontScale * 1.0);

			//cv::putText(imgFrame2Copy, std::to_string(i), blobs[i].centerPositions.back(), intFontFace, dblFontScale, SCALAR_GREEN, intFontThickness);
		}

		if ((blobs[i].blnStillBeingTracked == true) && (blobs[i].blnStopped == true)) {
			cv::rectangle(imgFrame2Copy, blobs[i].currentBoundingRect, SCALAR_YELLOW, 2);
		}

	}
}




static inline int abs_uint(const int i)
{
	return (i >= 0) ? i : -i;
}
static inline int32_t distance_is_close_16u_C3R(uint16_t r1, uint16_t g1, uint16_t b1, uint16_t r2, uint16_t g2, uint16_t b2, uint32_t threshold)
{
	return (abs_uint(r1 - r2) + abs_uint(g1 - g2) + abs_uint(b1 - b2) <= 4.2 * threshold);
}

struct vibeModel_Sequential
{
	/* Parameters. */
	uint32_t width;
	uint32_t height;
	uint32_t numberOfSamples;
	uint32_t matchingThreshold;
	uint32_t matchingNumber;
	uint32_t updateFactor;

	/* Storage for the history. */
	uint16_t *historyImage;
	uint16_t *historyBuffer;
	uint32_t lastHistoryImageSwapped;

	/* Buffers with random values. */
	uint32_t *jump;
	int *neighbor;
	uint32_t *position;
};
typedef struct vibeModel_Sequential vibeModel_Sequential_t;

vibeModel_Sequential_t *libvibeModel_Sequential_New()
{
	/* Model structure alloc. */
	vibeModel_Sequential_t *model = NULL;
	model = (vibeModel_Sequential_t*)calloc(1, sizeof(*model));
	assert(model != NULL);

	/* Default parametersB values. */
	model->numberOfSamples = 20;
	model->matchingThreshold = 120;
	model->matchingNumber = 4;
	model->updateFactor = 6060;

	/* Storage for the history. */
	model->historyImage = NULL;
	model->historyBuffer = NULL;
	model->lastHistoryImageSwapped = 0;

	/* Buffers with random values. */
	model->jump = NULL;
	model->neighbor = NULL;
	model->position = NULL;

	return(model);
}

// -----------------------------------------------------------------------------
// Some "Get-ers"
// -----------------------------------------------------------------------------
uint32_t libvibeModel_Sequential_GetNumberOfSamples(const vibeModel_Sequential_t *model)
{
	assert(model != NULL); return(model->numberOfSamples);
}

uint32_t libvibeModel_Sequential_GetMatchingNumber(const vibeModel_Sequential_t *model)
{
	assert(model != NULL); return(model->matchingNumber);
}

uint32_t libvibeModel_Sequential_GetMatchingThreshold(const vibeModel_Sequential_t *model)
{
	assert(model != NULL); return(model->matchingThreshold);
}

uint32_t libvibeModel_Sequential_GetUpdateFactor(const vibeModel_Sequential_t *model)
{
	assert(model != NULL); return(model->updateFactor);
}

// -----------------------------------------------------------------------------
// Some "Set-ers"
// -----------------------------------------------------------------------------
int32_t libvibeModel_Sequential_SetMatchingThreshold(
	vibeModel_Sequential_t *model,
	const uint32_t matchingThreshold
) {
	assert(model != NULL);
	assert(matchingThreshold > 0);

	model->matchingThreshold = matchingThreshold;

	return(0);
}

// -----------------------------------------------------------------------------
int32_t libvibeModel_Sequential_SetMatchingNumber(
	vibeModel_Sequential_t *model,
	const uint32_t matchingNumber
) {
	assert(model != NULL);
	assert(matchingNumber > 0);

	model->matchingNumber = matchingNumber;

	return(0);
}

// -----------------------------------------------------------------------------
int32_t libvibeModel_Sequential_SetUpdateFactor(
	vibeModel_Sequential_t *model,
	const uint32_t updateFactor
) {
	assert(model != NULL);
	assert(updateFactor > 0);

	model->updateFactor = updateFactor;

	/* We also need to change the values of the jump buffer ! */
	assert(model->jump != NULL);

	/* Shifts. */
	int size = (model->width > model->height) ? 2 * model->width + 1 : 2 * model->height + 1;

	for (int i = 0; i < size; ++i)
		model->jump[i] = (updateFactor == 1) ? 1 : (rand() % (2 * model->updateFactor)) + 1; // 1 or values between 1 and 2 * updateFactor.

	return(0);
}

// ----------------------------------------------------------------------------
// Frees the structure
// ----------------------------------------------------------------------------
int32_t libvibeModel_Sequential_Free(vibeModel_Sequential_t *model)
{
	if (model == NULL)
		return(-1);

	if (model->historyBuffer == NULL) {
		free(model);
		return(0);
	}

	free(model->historyImage);
	free(model->historyBuffer);
	free(model->jump);
	free(model->neighbor);
	free(model->position);
	free(model);

	return(0);
}



int euclidianDist(CvPoint i, CvPoint s) {

	return (int)sqrt((double)(i.x - s.x)*(i.x - s.x) + (double)(i.y - s.y)*(i.y - s.y));
}


// -----------------------------------------------------------------------------
// Allocates and initializes a C1R model structure
// -----------------------------------------------------------------------------
int32_t libvibeModel_Sequential_AllocInit_16u_C1R(
	vibeModel_Sequential_t *model,
	const uint16_t *image_data,
	const uint32_t width,
	const uint32_t height
) {
	/* Some basic checks. */
	assert((image_data != NULL) && (model != NULL));
	assert((width > 0) && (height > 0));

	/* Finish model alloc - parameters values cannot be changed anymore. */
	model->width = width;
	model->height = height;

	/* Creates the historyImage structure. */
	model->historyImage = NULL;
	model->historyImage = (uint16_t*)malloc(NUMBER_OF_HISTORY_IMAGES * width * height * sizeof(uint16_t));

	assert(model->historyImage != NULL);

	for (int i = 0; i < NUMBER_OF_HISTORY_IMAGES; ++i) {
		for (int index = width * height - 1; index >= 0; --index)
			model->historyImage[i * width * height + index] = image_data[index];
	}

	/* Now creates and fills the history buffer. */
	model->historyBuffer = (uint16_t*)malloc(width * height * (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES) * sizeof(uint16_t));
	assert(model->historyBuffer != NULL);

	for (int index = width * height - 1; index >= 0; --index) {
		uint16_t value = image_data[index];

		for (int x = 0; x < (int)model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES; ++x) {
			int value_plus_noise = (int)value + rand() % (int)((20.0 / 255.0) * 60000) - (int)((10.0 / 255.0) * 60000);

			if (value_plus_noise < 0) { value_plus_noise = 0; }
			if (value_plus_noise > 60000) { value_plus_noise = 60000; }

			model->historyBuffer[index * (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES) + x] = value_plus_noise;
		}
	}

	/* Fills the buffers with random values. */
	int size = (width > height) ? 2 * width + 1 : 2 * height + 1;

	model->jump = (uint32_t*)malloc(size * sizeof(*(model->jump)));
	assert(model->jump != NULL);

	model->neighbor = (int*)malloc(size * sizeof(*(model->neighbor)));
	assert(model->neighbor != NULL);

	model->position = (uint32_t*)malloc(size * sizeof(*(model->position)));
	assert(model->position != NULL);

	for (int i = 0; i < size; ++i) {
		model->jump[i] = (rand() % (2 * model->updateFactor)) + 1;            // Values between 1 and 2 * updateFactor.
		model->neighbor[i] = ((rand() % 3) - 1) + ((rand() % 3) - 1) * width / 240; // Values between { -width - 1, ... , width + 1 }.
		model->position[i] = rand() % (model->numberOfSamples);               // Values between 0 and numberOfSamples - 1.
	}

	return(0);
}

// -----------------------------------------------------------------------------
// Segmentation of a C1R model
// -----------------------------------------------------------------------------
int32_t libvibeModel_Sequential_Segmentation_16u_C1R(
	vibeModel_Sequential_t *model,
	const uint16_t *image_data,
	uint8_t *segmentation_map
) {
	/* Basic checks. */
	assert((image_data != NULL) && (model != NULL) && (segmentation_map != NULL));
	assert((model->width > 0) && (model->height > 0));
	assert(model->historyBuffer != NULL);
	assert((model->jump != NULL) && (model->neighbor != NULL) && (model->position != NULL));

	/* Some variables. */
	uint32_t width = model->width;
	uint32_t height = model->height;
	uint32_t matchingNumber = model->matchingNumber;
	uint32_t matchingThreshold = model->matchingThreshold;

	uint16_t *historyImage = model->historyImage;
	uint16_t *historyBuffer = model->historyBuffer;

	/* Segmentation. */
	//memset(segmentation_map, matchingNumber - 1, width * height);

	for (int i = 0; i < (int)(width*height); i++)
		segmentation_map[i] = matchingNumber - 1;

	/* First history Image structure. */
	for (int index = (int)(width * height - 1); index >= 0; --index) {
		if (abs_uint((int)(image_data[index] - historyImage[index])) > (int)matchingThreshold)
			segmentation_map[index] = matchingNumber;
	}

	/* Next historyImages. */
	for (int i = 1; i < NUMBER_OF_HISTORY_IMAGES; ++i) {
		uint16_t *pels = &historyImage[i * width * height];

		for (int index = width * height - 1; index >= 0; --index) {
			if (abs_uint(image_data[index] - pels[index]) <= (int)matchingThreshold)
				--segmentation_map[index];
		}
	}

	/* For swapping. */
	model->lastHistoryImageSwapped = (model->lastHistoryImageSwapped + 1) % NUMBER_OF_HISTORY_IMAGES;
	uint16_t *swappingImageBuffer = &historyImage[(model->lastHistoryImageSwapped) * width * height];

	/* Now, we move in the buffer and leave the historyImages. */
	int numberOfTests = (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES);

	for (int index = width * height - 1; index >= 0; --index) {
		if (segmentation_map[index] > 0) {
			/* We need to check the full border and swap values with the first or second historyImage.
			* We still need to find a match before we can stop our search.
			*/
			uint32_t indexHistoryBuffer = index * numberOfTests;
			uint16_t currentValue = image_data[index];

			for (int i = numberOfTests; i > 0; --i, ++indexHistoryBuffer) {
				if (abs_uint(currentValue - historyBuffer[indexHistoryBuffer]) <= (int)matchingThreshold) {
					--segmentation_map[index];

					/* Swaping: Putting found value in history image buffer. */
					uint16_t temp = swappingImageBuffer[index];
					swappingImageBuffer[index] = historyBuffer[indexHistoryBuffer];
					historyBuffer[indexHistoryBuffer] = temp;

					/* Exit inner loop. */
					if (segmentation_map[index] <= 0) break;
				}
			} // for
		} // if
	} // for

	  /* Produces the output. Note that this step is application-dependent. */
	  //for (uint16_t *mask = segmentation_map; mask < segmentation_map + (width * height); ++mask)
	  //	if (*mask > 0) *mask = COLOR_FOREGROUND;

	for (int i = 0; i < (int)(width*height); i++) {
		if (segmentation_map[i] > 0) segmentation_map[i] = COLOR_FOREGROUND;
	}

	return(0);
}

// ----------------------------------------------------------------------------
// Update a C1R model
// ----------------------------------------------------------------------------
int32_t libvibeModel_Sequential_Update_16u_C1R(
	vibeModel_Sequential_t *model,
	const uint16_t *image_data,
	uint8_t *updating_mask
) {
	/* Basic checks . */
	assert((image_data != NULL) && (model != NULL) && (updating_mask != NULL));
	assert((model->width > 0) && (model->height > 0));
	assert(model->historyBuffer != NULL);
	assert((model->jump != NULL) && (model->neighbor != NULL) && (model->position != NULL));

	/* Some variables. */
	uint32_t width = model->width;
	uint32_t height = model->height;

	uint16_t *historyImage = model->historyImage;
	uint16_t *historyBuffer = model->historyBuffer;

	/* Some utility variable. */
	int numberOfTests = (model->numberOfSamples - NUMBER_OF_HISTORY_IMAGES);

	/* Updating. */
	uint32_t *jump = model->jump;
	int *neighbor = model->neighbor;
	uint32_t *position = model->position;

	/* All the frame, except the border. */
	uint32_t shift, indX, indY;
	int x, y;

	for (y = 1; y < (int)(height - 1); ++y) {
		shift = rand() % width;
		indX = jump[shift]; // index_jump should never be zero (> 1).

		while (indX < width - 1) {
			int index = indX + y * width;

			if (updating_mask[index] == COLOR_BACKGROUND) {
				/* In-place substitution. */
				uint16_t value = image_data[index];
				int index_neighbor = index + neighbor[shift];

				if (position[shift] < NUMBER_OF_HISTORY_IMAGES) {
					historyImage[index + position[shift] * width * height] = value;
					historyImage[index_neighbor + position[shift] * width * height] = value;
				}
				else {
					int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
					historyBuffer[index * numberOfTests + pos] = value;
					historyBuffer[index_neighbor * numberOfTests + pos] = value;
				}
			}

			++shift;
			indX += jump[shift];
		}
	}

	/* First row. */
	y = 0;
	shift = rand() % width;
	indX = jump[shift]; // index_jump should never be zero (> 1).

	while (indX <= width - 1) {
		int index = indX + y * width;

		if (updating_mask[index] == COLOR_BK) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indX += jump[shift];
	}

	/* Last row. */
	y = height - 1;
	shift = rand() % width;
	indX = jump[shift]; // index_jump should never be zero (> 1).

	while (indX <= width - 1) {
		int index = indX + y * width;

		if (updating_mask[index] == COLOR_BACKGROUND) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indX += jump[shift];
	}

	/* First column. */
	x = 0;
	shift = rand() % height;
	indY = jump[shift]; // index_jump should never be zero (> 1).

	while (indY <= height - 1) {
		int index = x + indY * width;

		if (updating_mask[index] == COLOR_BACKGROUND) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indY += jump[shift];
	}

	/* Last column. */
	x = width - 1;
	shift = rand() % height;
	indY = jump[shift]; // index_jump should never be zero (> 1).

	while (indY <= height - 1) {
		int index = x + indY * width;

		if (updating_mask[index] == COLOR_BACKGROUND) {
			if (position[shift] < NUMBER_OF_HISTORY_IMAGES)
				historyImage[index + position[shift] * width * height] = image_data[index];
			else {
				int pos = position[shift] - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[index * numberOfTests + pos] = image_data[index];
			}
		}

		++shift;
		indY += jump[shift];
	}

	/* The first pixel! */
	if (rand() % model->updateFactor == 0) {
		if (updating_mask[0] == 0) {
			int position = rand() % model->numberOfSamples;

			if (position < NUMBER_OF_HISTORY_IMAGES)
				historyImage[position * width * height] = image_data[0];
			else {
				int pos = position - NUMBER_OF_HISTORY_IMAGES;
				historyBuffer[pos] = image_data[0];
			}
		}
	}

	return(0);
}

void Erosion(int erosion_type, int erosion_size, Mat &e)
{

	Mat element = getStructuringElement(erosion_type,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));

	/// Apply the erosion operation
	erode(e, e, element);
}
void Dilation(int dilation_type, int dilation_size, Mat &d)
{


	Mat element = getStructuringElement(dilation_type,
		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		Point(dilation_size, dilation_size));
	/// Apply the dilation operation
	dilate(d, d, element);

}

void processMotion(string fname)
{
	/* Create the capture object. */
	//VideoCapture capture(videoFilename);

	ifstream ifiles;
	string line;
	stringstream convert;
	Rect roi;

	/* Variables. */
	static int frameNumber = 1; /* The current frame number */
	Mat i_image;
	Mat image;                  /* Current frame. */
	Mat image_g;
	Mat segmentationMap;        /* Will contain the segmentation map. This is the binary output map. */
	int keyboard = 0;           /* Input from keyboard. Used to stop the program. Enter 'q' to quit. */
	Mat bgr[3];
	/* Model for ViBe. */
	vibeModel_Sequential_t *model = NULL; /* Model used by ViBe. */
	ifiles.open(fname, ifstream::in);

	SimpleBlobDetector::Params params;

	// Change thresholds
	//params.minThreshold =50;
	//params.maxThreshold = 180;
	params.minDistBetweenBlobs = 1.5f;
	params.blobColor = 255;
	// Filter by Area.
	params.filterByArea = false;
	params.minArea = 1.3f;

	// Filter by Circularity
	params.filterByCircularity = false;
	params.minCircularity = 0.8f;

	// Filter by Convexity
	params.filterByConvexity = false;
	params.minConvexity = 1.0f;

	// Filter by Inertia
	params.filterByInertia = false;
	params.minInertiaRatio = 0.1f;
	// Set up the detector with default parameters.
	//SimpleBlobDetector detector;
	Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);

	int b_number = 0;
	std::vector<Blob> blobs;

	if (ifiles.is_open())
	{
		//	getline(ifiles, line);
		//	Mat ii = imread(line, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);

		int bead_count = 0;
		std::vector<Blob> currentFrameBlobs;
		std::vector<Point2f> predBlobPts;
		std::vector<Blob> previousFrameBlobs;
		std::vector<vector<double>> costMatrix;
		std::vector<int> assignment;
		std::set<int> unmatchedDetections;
		std::set<int> unmatchedTrajectories;
		std::set<int> allItems;
		std::set<int> matchedItems;
		std::vector<cv::Point> matchedPairs;

		unsigned int trkNum = 0;
		unsigned int detNum = 0;
		int bead_id = 0;

		while (ifiles.good())
		{
			FILETIME ft;
			LARGE_INTEGER li;


			getline(ifiles, line);
			image = imread(line, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
			//image = i_image(Rect(10, 10, 1590, 1180));

			if ((frameNumber % 50) == 0) { cout << "Frame number = " << frameNumber << endl; }

			/* Applying ViBe.
			* If you want to use the grayscale version of ViBe (which is much faster!):
			* (1) remplace C3R by C1R in this file.
			* (2) uncomment the next line (cvtColor).
			*/

			GetSystemTimeAsFileTime(&ft);
			li.LowPart = ft.dwLowDateTime;
			li.HighPart = ft.dwHighDateTime;

			uint64_t t0 = li.QuadPart;
			t0 -= 116444736000000000LL; /* Convert from file time to UNIX epoch time. */
			t0 /= 10000; /* From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals */

			cvtColor(image, image_g, CV_BGR2GRAY);

			Mat frame(image_g.rows / 4, image_g.cols / 4, CV_16UC1, Scalar(0));
			resize(image_g, frame, frame.size(), 0, 0, INTER_AREA);

			if (frameNumber == 1) {
				segmentationMap = Mat(frame.rows, frame.cols, CV_8UC1, Scalar(0));
				model = (vibeModel_Sequential_t*)libvibeModel_Sequential_New();
				libvibeModel_Sequential_AllocInit_16u_C1R(model, (uint16_t *)frame.data, frame.cols, frame.rows);
			}

			/* ViBe: Segmentation and updating. */
			libvibeModel_Sequential_Segmentation_16u_C1R(model, (uint16_t *)frame.data, (uint8_t *)segmentationMap.data);
			libvibeModel_Sequential_Update_16u_C1R(model, (uint16_t *)frame.data, (uint8_t *)segmentationMap.data);

			Erosion(MORPH_CROSS, 1, segmentationMap);
			Dilation(MORPH_ELLIPSE, 1, segmentationMap);

			std::vector<KeyPoint> keypoints;
			detector->detect(segmentationMap, keypoints);


			if (frameNumber == 1) {
				for (auto &kp : keypoints) {
					Blob b(kp.pt);

					b.id = bead_id++;
					previousFrameBlobs.push_back(b);

				}
			}
			else {
				currentFrameBlobs.clear();
				predBlobPts.clear();

				//predict next pointd
				for (auto &pb : previousFrameBlobs) {

					predBlobPts.push_back(pb.predict());
				}
				// new current blobs using kepointd
				for (auto &kp : keypoints) {

					//if (kp.pt.x > 40) {
					Blob b(kp.pt);
					bool add_v = true;
					for (auto &pb : previousFrameBlobs) {
						//b.id = pb.id;
						float pbx = (float)pb.centerPositions[0].x;
						float pby = (float)pb.centerPositions[0].y;


						if ((abs(kp.pt.x - pbx) < 3) && (abs(kp.pt.y - pby) < 3))
						{
							b.dx = pb.dx;
							add_v = false;
						}

					}
					if (add_v) {
						b.blnStopped = false;
					}
					else {
						b.blnStopped = true;
					}

					currentFrameBlobs.push_back(b);
					//}
				}


				trkNum = (unsigned int)predBlobPts.size();
				detNum = (unsigned int)currentFrameBlobs.size();

				costMatrix.clear();
				costMatrix.resize(trkNum, vector<double>(detNum, 0));

				for (unsigned int i = 0; i < trkNum; i++) // compute iou matrix as a distance matrix
				{
					for (unsigned int j = 0; j < detNum; j++)
					{
						// use 1-iou because the hungarian algorithm computes a minimum-cost assignment.
						if ((!currentFrameBlobs[j].blnStopped) && !(previousFrameBlobs[i].blnStopped)) {
							costMatrix[i][j] = GetCost(predBlobPts[i], currentFrameBlobs[j].centerPositions[0]);
						}
						else {
							costMatrix[i][j] = 0;
						}
					}
				}

				//compute assignment
				HungarianAlgorithm HungAlgo;
				assignment.clear();
				HungAlgo.Solve(costMatrix, assignment);

				unmatchedTrajectories.insert(0);
				unmatchedTrajectories.clear();
				unmatchedDetections.clear();
				allItems.clear();
				matchedItems.clear();

				if (detNum > trkNum) //	there are unmatched detections
				{
					for (unsigned int n = 0; n < detNum; n++)
						allItems.insert(n);

					for (unsigned int i = 0; i < trkNum; ++i)
						matchedItems.insert(assignment[i]);

					set_difference(allItems.begin(), allItems.end(),
						matchedItems.begin(), matchedItems.end(),
						insert_iterator<set<int>>(unmatchedDetections, unmatchedDetections.begin()));
				}
				else if (detNum < trkNum) // there are unmatched trajectory/predictions
				{
					for (unsigned int i = 0; i < trkNum; ++i)
						if (assignment[i] == -1) // unassigned label will be set as -1 in the assignment algorithm
							unmatchedTrajectories.insert(i);
				}
				else
					;

				matchedPairs.clear();
				for (unsigned int i = 0; i < trkNum; ++i)
				{
					if (assignment[i] == -1) // pass over invalid values
						continue;
					if (costMatrix[i][assignment[i]] < 0.10)
					{
						unmatchedTrajectories.insert(i);
						unmatchedDetections.insert(assignment[i]);
					}
					else
						matchedPairs.push_back(cv::Point(i, assignment[i]));
				}

			}


			/* Shows the current frame and the segmentation map. */
			Mat im_with_keypoints;
			drawKeypoints(segmentationMap, keypoints, im_with_keypoints, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);


			int detIdx, trkIdx;
			for (unsigned int i = 0; i < matchedPairs.size(); i++)
			{
				trkIdx = matchedPairs[i].x;
				detIdx = matchedPairs[i].y;



				float kpx = keypoints[detIdx].pt.x;
				float kpy = keypoints[detIdx].pt.y;
				float pfx = (float)previousFrameBlobs[trkIdx].centerPositions[0].x;
				float pfy = (float)previousFrameBlobs[trkIdx].centerPositions[0].y;
				bool stopped = false;

				if ((abs(kpx - pfx) < 5) && (abs(kpy - pfy) < 5))
					stopped = true;

				if ((!previousFrameBlobs[trkIdx].blnStopped) && (!stopped) && (keypoints[detIdx].pt.x > 40) && (previousFrameBlobs[trkIdx].centerPositions[0].x > 40)) {
					cv::line(im_with_keypoints, previousFrameBlobs[trkIdx].centerPositions[0], keypoints[detIdx].pt, Scalar(0, 255, 0), 1, 8, 0);
					convert << previousFrameBlobs[trkIdx].id;
					cv::putText(im_with_keypoints, convert.str(), keypoints[detIdx].pt, FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 0), 1, 8, false);
					convert.clear();
					convert.str(string());
				}
			}
			if (frameNumber > 1) {
				vector<Blob> pfb = previousFrameBlobs;
				previousFrameBlobs.clear();


				for (unsigned int i = 0; i < matchedPairs.size(); i++)
				{
					trkIdx = matchedPairs[i].x;
					detIdx = matchedPairs[i].y;

					//			currentFrameBlobs[detIdx].centerPositions[0].x = pfb[trkIdx].centerPositions[0].x;
					//			currentFrameBlobs[detIdx].centerPositions[0].y = pfb[trkIdx].centerPositions[0].y;
					currentFrameBlobs[detIdx].update(keypoints[detIdx].pt);
					currentFrameBlobs[detIdx].id = pfb[trkIdx].id;
					currentFrameBlobs[detIdx].dx = pfb[trkIdx].dx;
					currentFrameBlobs[detIdx].blnStopped = pfb[trkIdx].blnStopped;

					currentFrameBlobs[detIdx].frame = pfb[trkIdx].frame + 1;

					if ((currentFrameBlobs[detIdx].centerPositions[0].x > 40)) {
						previousFrameBlobs.push_back(currentFrameBlobs[detIdx]);
					}
					else if (!currentFrameBlobs[detIdx].blnStopped) {
						bead_count++;
					}
				}
				for (auto umd : unmatchedDetections)
				{
					//KalmanTracker tracker = KalmanTracker(detFrameData[fi][umd].box);
					//trackers.push_back(tracker);
					//if ((keypoints[umd].pt.x > 40)) {
					Blob b(keypoints[umd].pt);
					bool add_v = true;
					for (auto &pb : previousFrameBlobs) {
						//b.id = pb.id;
						float pbx = (float)pb.centerPositions[0].x;
						float pby = (float)pb.centerPositions[0].y;

						//
						if ((abs(keypoints[umd].pt.x - pbx) < 3) && (abs(keypoints[umd].pt.y - pby) < 3)) {
							b.dx = pb.dx;
							b.id = pfb[umd].id;


							add_v = false;
						}

					}
					if (add_v) {
						b.blnStopped = false;
					}
					else {
						b.blnStopped = true;
					}

					if ((currentFrameBlobs[umd].id == 0)&&(!b.blnStopped)){
						b.id = bead_id++;
					}
					else {
						b.id = currentFrameBlobs[umd].id;
						b.blnStopped = currentFrameBlobs[umd].blnStopped;
					}
					//b.id = pfb[umd].id;
					//	b.blnStopped = pfb[umd].blnStopped;
					//if(b.)
					if (b.centerPositions[0].x > 40) {
						previousFrameBlobs.push_back(b);
					}

					else {
						bead_count++;
					}
				}
			}

			GetSystemTimeAsFileTime(&ft);
			li.LowPart = ft.dwLowDateTime;
			li.HighPart = ft.dwHighDateTime;

			uint64 t1 = li.QuadPart;
			t1 -= 116444736000000000LL; /* Convert from file time to UNIX epoch time. */
			t1 /= 10000; /* From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals */

			uint64 t = t1 - t0;

			if (frameNumber % 2 == 0) {
				for (auto &b : blobs) {
					if (b.blnStillBeingTracked && !(b.blnStopped)) {
						roi.x = ((int)b.centerPositions[0].x) * 4 - 10;
						roi.y = ((int)b.centerPositions[0].y) * 4 - 10;

						roi.width = 20;
						roi.height = 20;
						if ((roi.x > 0) && (roi.y > 0) && (roi.x + roi.width < image.cols) && (roi.y + roi.height < image.rows)) {
							Mat bead = image(roi);

							split(bead, bgr);

							cout << (unsigned short)b.centerPositions[0].x << "  " << (unsigned short)b.centerPositions[0].y << endl;

							bgr[0].at<unsigned short>(0, 0) = (unsigned short)b.centerPositions[0].x * 4;
							bgr[0].at<unsigned short>(0, 1) = (unsigned short)b.centerPositions[0].y * 4;

							vector<Mat> array_to_merge;
							array_to_merge.push_back(bgr[0]);
							array_to_merge.push_back(bgr[1]);
							array_to_merge.push_back(bgr[2]);

							merge(array_to_merge, bead);

							convert << "capture//i_" << b_number++ << ".tiff";
							string fn = convert.str();
							convert.clear();//clear any bits set
							convert.str(string());


							vector<int> params = {
								259, 1//,     // No compression, turn off default LZW 
							};
							Scalar s = sum(bead);
							if ((s(2) > 20000) || (s(1) > 12000) || (s(0) > 16000)) {
								//				if (bead.data)
								//					imwrite(fn, bead, params);
							}
						}
					}
				}
			}


			convert << line << "  " << t << "  " << currentFrameBlobs.size() << " " << bead_count;
			string hd = convert.str();
			convert.clear();//clear any bits set
			convert.str(string());
			namedWindow("Segmentation by ViBe", CV_WINDOW_NORMAL);// Create a window for display.
																  //resizeWindow("Segmentation by ViBe", (int)(segmentationMap.cols / 4), (int)(segmentationMap.rows / 4));



			resizeWindow("Segmentation by ViBe", (int)(frame.cols*1.2), (int)(frame.rows*1.2));
			putText(/*segmentationMap*/im_with_keypoints, hd, CvPoint(10, 220), FONT_HERSHEY_SIMPLEX, .3,
				Scalar::all(65535), 1, 8);
			imshow("Segmentation by ViBe", /*segmentationMap*/im_with_keypoints);
			//currentFrameBlobs.clear();
			++frameNumber;



			/* Gets the input from the keyboard. */
			keyboard = waitKey(1);

		}
	}


	/* Frees the model. */
	libvibeModel_Sequential_Free(model);
}


int main(int argc, char * argv[])
{
	if (argc < 2) {
		cout << "enter an input file name" << endl;
		return 1;
	}

	processMotion(string(argv[1]));
	return 0;
}

